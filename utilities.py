from constants import DB_ROOT
from datetime import datetime
from firebase_admin import auth


# Method to fetch the user details from the FB RT DB
def get_user(email):
    print("Getting Firebase user profile for the user {}".format(email))
    return auth.get_user_by_email(email)


# Return a chat_room skeleton
def get_new_chat_room(work_order_id, clinician, technician):
    chat_room, participants, messages = {}, {}, {}

    participants[clinician.uid] = clinician.email
    participants[technician.uid] = technician.email

    chat_room.update({
        'wo_id': work_order_id,
        'participants': participants,
        'conversation': messages
    })

    chat_room[clinician.uid] = {}
    chat_room[technician.uid] = {}

    return chat_room


# Return a message skeleton
def get_new_message(content, frm, type=None, action=None):
    msg_type = type or "text"
    message = {
        "from": frm,
        "time": str(datetime.now()),
        "content": content,
        "type": msg_type,
        "action": action
    }

    return message


# Return a DB resource
def get_db_ref_url(*args):
    db_url = DB_ROOT
    for arg in args:
        db_url = "{}/{}".format(db_url, arg)

    print("DB URL", db_url)
    return db_url


# Class that converts errors to json response
class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv
