from random import randint
from constants import DB_ROOT
from flask import Flask, jsonify, request
from firebase_admin import credentials, initialize_app, db
from constants import (
    TECHNICIAN_EMAIL, CLINICIAN_EMAIL, CLINICIAN, TECHNICIAN, SERVER
)
from utilities import (
    get_user, get_new_chat_room, InvalidUsage, get_new_message, get_db_ref_url
)

app = Flask(__name__)


# Register an error handler
@app.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# Load the certificate for the firebase service account
cred = credentials.Certificate(
    '/home/anshumansaagar/.ssh/creds/tekagogo-chat-box-2232a5f93404.json')

# Initialize the app with a service account, granting admin privileges
initialize_app(cred, {
    'databaseURL': 'https://tekagogo-chat-box.firebaseio.com/'
})

# As an admin, the app has access to read and write all data, regradless of
# Security Rules
db_ref = db.reference
ref = db_ref(DB_ROOT)

# Get the clinician and technician account details
# In production this data might have to be fetched from DB instead of from FB
clinician = get_user(CLINICIAN_EMAIL)
technician = get_user(TECHNICIAN_EMAIL)


@app.route('/create-new-chat-room', methods=["POST"])
def create_new_chat_room():
    work_order_id = randint(100000, 999999)
    chat_room_one = get_new_chat_room(
        work_order_id=work_order_id, clinician=clinician, technician=technician
    )

    chat_room_ref = ref.push(chat_room_one)
    chat_room_key = chat_room_ref.key

    return jsonify({"chat_room_key": chat_room_key})


@app.route('/post-a-message', methods=["POST"])
def post_a_message():

    map = {
        '{}-{}'.format(SERVER, CLINICIAN): clinician.uid,
        '{}-{}'.format(SERVER, TECHNICIAN): technician.uid,
        '{}-{}'.format(TECHNICIAN, CLINICIAN): 'messages',
        '{}-{}'.format(CLINICIAN, TECHNICIAN): 'messages',
        CLINICIAN: clinician.uid,
        TECHNICIAN: technician.uid,
        'server': 'tekagogo'
    }

    data = request.json
    frm = data.get('from')
    to = data.get('to')
    msg = data.get('message')
    chat_room_key = data.get('chat_room_key')

    actors = ['clinician', 'technician', 'server']

    if to not in actors[:2]:
        raise InvalidUsage(
            "'clincian' or 'technician' and that's it for a receiver!",
            status_code=400
        )

    elif frm not in actors:
        raise InvalidUsage(
            "{} only!".format(actors)
        )

    else:
        sender = map[frm]
        message = get_new_message(frm=sender, content=msg)

        db_url = get_db_ref_url(chat_room_key, map['{}-{}'.format(frm, to)])

        # I am going to access and change a global variable here
        # NEVER EVER EVER EVER EVER NEVER DO THIS
        # Peace!
        ref = db_ref(db_url)
        ref.push(message)

        print ("DB Reference", ref)
        return jsonify({"result": "To win is in our blood!"})


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=8000)


# # ------- Code to Create a user -------
# user = auth.create_user(
#     email="anshuman.saagar@outlook.com",
#     email_verified=True,
#     phone_number="+917892654021",
#     password="password",
#     display_name="Anshuman Technician",
#     disabled=False
# )

# print("User: {}".format(user.uid))
